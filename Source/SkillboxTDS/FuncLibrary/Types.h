// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Engine/DataTable.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

class ABulletDefault;
class AWeaponDefault;


UCLASS()
class SKILLBOXTDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Pistol UMETA(DisplayName = 'Pistol'),
	Rifle UMETA(DisplayName = 'Rifle'),
	Shotgun UMETA(DisplayName = 'Shotgun'),
	SniperRifle UMETA(DisplayName = 'SniperRifle'),
	GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncher UMETA(DisplayName = "RocketLauncher")
};

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	AimWalk_State UMETA(DisplayName = 'AimWalk State'),
	Aim_State UMETA(DisplayName = 'Aim State'),
	Walk_State UMETA(DisplayName = 'Walk State'),
	Run_State UMETA(DisplayName = 'Run State'),
	Sprint_State UMETA(DisplayName = "Sprint State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimWalkSpeed = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed = 800.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	//Bullets
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TSubclassOf<ABulletDefault> ProjectileTypes = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	UStaticMesh* ProjectileMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float ProjectileDamage = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float ProjectileLifeTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float ProjectileInitSpeed = 10000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	bool bIsLikeABomb = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float MaxRadiusFullDamage = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	bool bIsPenetrating = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile", meta = (EditCondition = "bIsPenetrating"))
	int32 NumberOfPenetrating = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	UParticleSystem* TrialFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	USoundBase* HitSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitDecalsFX;
	
	//Grenades
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float RadiusExplosion = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float CenterExplosion = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float DamageFalloff = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	float DamageExplosion = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	UParticleSystem* ExplosiveFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	USoundBase* ExplosiveSound = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	//Aim_Walk_State
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimWalkStateMinDispersion = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimWalkStateMaxDispersion = 0.6f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimWalkStateRecoilDispersion = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimWalkStateReductionDispersion = 0.3f;

	//Aim_State
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimStateMinDispersion = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimStateMaxDispersion = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimStateRecoilDispersion = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float AimStateReductionDispersion = 0.2f;

	//Walk_State
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float WalkStateMinDispersion = 0.6f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float WalkStateMaxDispersion = 1.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float WalkStateRecoilDispersion = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float WalkStateReductionDispersion = 0.1f;

	//Run_State
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float RunStateMinDispersion = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float RunStateMaxDispersion = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float RunStateRecoilDispersion = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponDispersion")
	float RunStateReductionDispersion = 0.1f;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
	int32 AmmoRound = 30;
};

USTRUCT(BlueprintType)
struct FAmmoInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponTypeAmmo = EWeaponType::Pistol;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 CurrentAmmoInClip = 30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 MaxAmmoInClip = 30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 CurrentClips = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 MaxClips = 5;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	//Class
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
	TSubclassOf<AWeaponDefault> WeaponType;

	//AmmoInfo
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	FAmmoInfo AmmoInfo;
	
	//FireState
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireState")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireState")
	float ReloadTime = 1.5f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireState")
	//int32 MaxRound = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireState")
	int32 NumbersOfBullets = 1;

	//Dispersion
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	FWeaponDispersion FWeaponDispersion;

	//Projectile
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	FProjectileInfo ProjectileInfo;

	//Trace
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
	float WeaponDamage = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
	float TraceDistance = 1500.0f;
	
	//Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundReloadWeapon = nullptr;

	//FX
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	UParticleSystem* EffectFireWeapon = nullptr;
	
	//HitDecalEffect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Decal")
	UDecalComponent* DecalOnHit = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Decal")
	UMaterialInterface* DecalOnHitMat = nullptr;
	
	//Animation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	UAnimMontage* HipFireAnim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	UAnimMontage* IronsightFireAnim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	UAnimMontage* ReloadAnim = nullptr;

	//Accessories meshes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMesh* MagazineDrop = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMesh* SleeveBullets = nullptr;

	//Inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	float SwitchTimeToWeapon = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	EWeaponType WeaponTypeAmmo = EWeaponType::Rifle;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FName WeaponName;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "WeaponSlot")
	FWeaponInfo WeaponInfo;
	
};
