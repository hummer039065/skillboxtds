// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

DEFINE_LOG_CATEGORY_STATIC(InventoryComponentLog, All, All);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSwitchWeapon, FName, IdWeapon, const FAmmoInfo&, AmmoInfo);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, AmmoType, int32, AmmoCount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoInfoChange, int32, WeaponIndex, const FAmmoInfo, AmmoInfo);



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SKILLBOXTDS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnSwitchWeapon OnSwitchWeapon;
/**/	/*UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoChange OnAmmoChange;*/
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoInfoChange OnAmmoInfoChange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	TArray<FAmmoInfo> AmmoSlots;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	int32 MaxWeaponSlots = 0;

	bool SwitchWeaponToIndex(int32 NextIndex, int32 PrevIndex, const FAmmoInfo& PrevAmmoInfo);

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponSlotIndexByName(FName WeaponName);
	//FAdditionalWeaponInfo GetAdditionalWeaponInfoByIndex(int32 WeaponId);
	void SetWeaponInfoByIndex(int32 WeaponId, const FAmmoInfo& NewAmmoInfo);
	
	void WeaponChangeAmmo(EWeaponType TypeOfWeapon, const FAmmoInfo& AmmoInfo);

	UFUNCTION(BlueprintCallable)
	void UpdateWeaponInfo(const FWeaponInfo& WeaponInfo);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void InitInventoryWeapon();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
