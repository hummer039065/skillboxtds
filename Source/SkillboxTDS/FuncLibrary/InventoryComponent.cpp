// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillboxTDS/FuncLibrary/InventoryComponent.h"

#include "SkillboxTDS/Game/SkillboxTDSGameInstance.h"
#include "SkillboxTDS/Pickup/Weapons/WeaponDefault.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

bool UInventoryComponent::SwitchWeaponToIndex(int32 NextIndex, int32 PrevIndex, const FAmmoInfo& PrevAmmoInfo)
{
	bool bIsSuccess = false;
	int32 CorrectIndex = NextIndex;
	UE_LOG(InventoryComponentLog, Display, TEXT("CorrectIndex: %i"), CorrectIndex);
	UE_LOG(InventoryComponentLog, Display, TEXT("PrevAmmoInfo: %i"), PrevAmmoInfo.CurrentAmmoInClip);
	
	if (CorrectIndex > WeaponSlots.Num() - 1)
	{
		CorrectIndex = 0;
	}
	else if (CorrectIndex < 0)
	{
		CorrectIndex = WeaponSlots.Num() - 1;
	}

	FName NewIdName;
	FAmmoInfo AmmoInfo;
	
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		if (i == CorrectIndex)
		{
			if (!WeaponSlots[i].WeaponName.IsNone())
			{
				NewIdName = WeaponSlots[i].WeaponName;
				AmmoInfo = WeaponSlots[i].WeaponInfo.AmmoInfo;
				bIsSuccess = true;
			}
		}
		i++;
	}

	if (!bIsSuccess)
	{
		UE_LOG(InventoryComponentLog, Display, TEXT("Switch weapon faild!"));
	}

	if (bIsSuccess)
	{
		SetWeaponInfoByIndex(PrevIndex, PrevAmmoInfo);
		OnSwitchWeapon.Broadcast(NewIdName, AmmoInfo);
		UE_LOG(InventoryComponentLog, Display, TEXT("AmmoInfo: %i"), AmmoInfo.CurrentAmmoInClip);
	}

	/*if (!CanEquip()) return;
	
	CurrentWeaponIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
	EquipWeapon(CurrentWeaponIndex);*/


	
	return bIsSuccess;
}

int32 UInventoryComponent::GetWeaponSlotIndexByName(FName WeaponName)
{
	int32 result = -1;
	bool bIsFound = false;
	int32 i = 0;
	while (i < WeaponSlots.Num() && !bIsFound)
	{
		if (WeaponSlots[i].WeaponName == WeaponName)
		{
			bIsFound = true;
			result = i;
		}
		i++;
	}
	return result;
}


void UInventoryComponent::SetWeaponInfoByIndex(int32 WeaponId, const FAmmoInfo& NewAmmoInfo)
{
	if (WeaponSlots.IsValidIndex(WeaponId))
	{
		/*bool bIsFound = false;
		int32 i = 0;
		while (i < WeaponSlot.Num() && !bIsFound)
		{
			if (/*WeaponSlot[i].WeaponIndex#1#i == WeaponId)
			{
				WeaponSlot[i].WeaponInfo.AmmoInfo = NewAmmoInfo;
				bIsFound = true;

				OnAmmoInfoChange.Broadcast(WeaponId, NewAmmoInfo);
			}
			i++;
		}*/
		WeaponSlots[WeaponId].WeaponInfo.AmmoInfo = NewAmmoInfo;
		OnAmmoInfoChange.Broadcast(WeaponId, NewAmmoInfo);
	}
}

void UInventoryComponent::WeaponChangeAmmo(EWeaponType TypeOfWeapon, const FAmmoInfo& AmmoInfo)
{
	//UE_LOG(InventoryComponentLog, Display, TEXT("---------------------------------------"));
	//UE_LOG(InventoryComponentLog, Display, TEXT("Enter in to Weapon change Ammo"));
	bool bIsFind = false;
	int32 i = 0;
	//UE_LOG(InventoryComponentLog, Display, TEXT("AmmoWasInClip: %i"), AmmoInfo.CurrentAmmoInClip);
	/*while (i < AmmoSlot.Num() && !bIsFind)
	{
		if (AmmoSlot[i].WeaponTypeAmmo == TypeOfWeapon)
		{
			AmmoSlot[i].CurrentAmmoInClip += AmmoTaken;
			
			if (AmmoSlot[i].CurrentAmmoInClip > AmmoSlot[i].MaxAmmoInClip)
			{
				AmmoSlot[i].CurrentAmmoInClip = AmmoSlot[i].MaxAmmoInClip;
				UE_LOG(InventoryComponentLog, Display, TEXT("AmmoSlot: %i"), AmmoSlot[i].CurrentAmmoInClip);
			}
			OnAmmoChange.Broadcast(TypeOfWeapon, AmmoSlot[i].CurrentAmmoInClip);
			UE_LOG(InventoryComponentLog, Display, TEXT("To Widget: %i"), AmmoSlot[i].CurrentAmmoInClip);
			bIsFind = true;
		}
		i++;
	}*/
	/*if (CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip == 0)
	{
		--CurrentWeaponInfo.AmmoInfo.CurrentClips;
	}*/

	/*auto ChangedAmmoInfo = AmmoInfo;

	if (ChangedAmmoInfo.CurrentClips == 0)
	{
		UE_LOG(InventoryComponentLog, Display, TEXT("Ammo is Empty!"));
	}
	else
	{
		ChangedAmmoInfo.CurrentAmmoInClip = AmmoInfo.MaxAmmoInClip;
	}

	for (auto Weapon : WeaponSlots)
	{
		if (Weapon.WeaponInfo.WeaponTypeAmmo == TypeOfWeapon)
		{
			Weapon.WeaponInfo.AmmoInfo = ChangedAmmoInfo;
			OnAmmoChange.Broadcast(TypeOfWeapon, Weapon.WeaponInfo.AmmoInfo.CurrentAmmoInClip);
		}
	}*/

	for (auto Weapon : WeaponSlots)
	{
		if (Weapon.WeaponInfo.WeaponTypeAmmo == TypeOfWeapon)
		{
			Weapon.WeaponInfo.AmmoInfo = AmmoInfo;
			
			Weapon.WeaponInfo.AmmoInfo.CurrentAmmoInClip = AmmoInfo.MaxAmmoInClip;
			Weapon.WeaponInfo.AmmoInfo.CurrentAmmoInClip = 15;
			Weapon.WeaponInfo.AmmoInfo.CurrentClips = 3;
			
			OnAmmoInfoChange.Broadcast(GetWeaponSlotIndexByName(Weapon.WeaponName), Weapon.WeaponInfo.AmmoInfo);

			//UE_LOG(InventoryComponentLog, Display, TEXT("Weapon founded! Current weapon ammo: %i"), Weapon.WeaponInfo.AmmoInfo.CurrentAmmoInClip);
			//UE_LOG(InventoryComponentLog, Display, TEXT("Weapon founded! AmmoInfo: %i"), AmmoInfo.CurrentAmmoInClip);
			//UE_LOG(InventoryComponentLog, Display, TEXT("Weapon founded!"));
		}
	}
	
	UE_LOG(InventoryComponentLog, Display, TEXT("Weapon change ammo!"));
	
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{

	InitInventoryWeapon();
	
	Super::BeginPlay();
}

void UInventoryComponent::InitInventoryWeapon()
{
	for (int32 i = 0; i < WeaponSlots.Num(); i++)
	{
		auto MyGI = Cast<USkillboxTDSGameInstance>(GetWorld()->GetGameInstance());
		if (MyGI)
		{
			if (!WeaponSlots[i].WeaponName.IsNone())
			{
				FWeaponInfo WeaponInfo;
				if (MyGI->GetWeaponInfoByTableName(WeaponSlots[i].WeaponName, WeaponInfo))
				{
					//WeaponSlot[i].AdditionalWeaponInfo.AmmoRound = WeaponInfo.MaxRound;
					//WeaponSlot[i].WeaponInfo.AmmoInfo.CurrentAmmoInClip = WeaponInfo.AmmoInfo.MaxAmmoInClip;
					WeaponSlots[i].WeaponInfo = WeaponInfo;
				}
				else
				{
					WeaponSlots.RemoveAt(i);
				}
			}
		}
	}

	MaxWeaponSlots = WeaponSlots.Num();
	
	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].WeaponName.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].WeaponName, WeaponSlots[0].WeaponInfo.AmmoInfo);
		}
	}
	
}

void UInventoryComponent::UpdateWeaponInfo(const FWeaponInfo& WeaponInfo)
{
	for (auto Weapon : WeaponSlots)
	{
		if (Weapon.WeaponInfo.WeaponType == WeaponInfo.WeaponType)
		{
			Weapon.WeaponInfo = WeaponInfo;
			OnAmmoInfoChange.Broadcast(GetWeaponSlotIndexByName(Weapon.WeaponName), Weapon.WeaponInfo.AmmoInfo);
			UE_LOG(InventoryComponentLog, Display, TEXT("Weapon Info Updated!"));
		}
	}
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

