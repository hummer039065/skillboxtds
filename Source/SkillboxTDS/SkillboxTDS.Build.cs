// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SkillboxTDS : ModuleRules
{
	public SkillboxTDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] 
	        { "Core", 
		        "CoreUObject", 
		        "Engine", 
		        "InputCore", 
		        "HeadMountedDisplay", 
		        "NavigationSystem", 
		        "AIModule",
		        "PhysicsCore"
	        });
    }
}
