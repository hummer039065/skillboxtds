// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillboxTDS/Pickup/Weapons/Drops.h"

// Sets default values
ADrops::ADrops()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>("Collision");
	Collision->SetSphereRadius(SphereRadius);
	SetRootComponent(Collision);
	
	RoundDropModel = CreateDefaultSubobject<UStaticMeshComponent>("RoundModel");
	RoundDropModel->SetupAttachment(GetRootComponent());

	ShellDropModel = CreateDefaultSubobject<UStaticMeshComponent>("ShellModel");
	ShellDropModel->SetupAttachment(GetRootComponent());
	
}

// Called when the game starts or when spawned
void ADrops::BeginPlay()
{
	Super::BeginPlay();
	this->SetLifeSpan(5.0f);
	
}

// Called every frame
void ADrops::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

