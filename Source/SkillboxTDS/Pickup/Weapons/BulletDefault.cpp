// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillboxTDS/Pickup/Weapons/BulletDefault.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(DefaultBulletLog, All, All);

// Sets default values
ABulletDefault::ABulletDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(SphereCollision);
	SphereCollision->SetSphereRadius(16.0f);
	SphereCollision->SetCanEverAffectNavigation(true);
	SphereCollision->bReturnMaterialOnMove = true;
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BulletModel"));
	StaticMesh->SetCanEverAffectNavigation(true);
	StaticMesh->SetupAttachment(SphereCollision);

	ProjectileFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BulletFX"));
	ProjectileFX->SetupAttachment(SphereCollision);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovementComponent->SetUpdatedComponent(RootComponent);
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	//ProjectileMovementComponent->MaxSpeed = CurrentProjectileInfo.ProjectileInitSpeed;
	//ProjectileMovementComponent->InitialSpeed = CurrentProjectileInfo.ProjectileInitSpeed;
}

void ABulletDefault::InitProjectile()
{
	this->SetLifeSpan(CurrentProjectileInfo.ProjectileLifeTime);
	ProjectileMovementComponent->InitialSpeed = CurrentProjectileInfo.ProjectileInitSpeed;
	ProjectileMovementComponent->MaxSpeed = CurrentProjectileInfo.ProjectileInitSpeed;
}

// Called when the game starts or when spawned
void ABulletDefault::BeginPlay()
{
	Super::BeginPlay();
	InitProjectile();
	
	SphereCollision->OnComponentHit.AddDynamic(this, &ABulletDefault::BulletOnComponentHit);
}

// Called every frame
void ABulletDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABulletDefault::BulletOnComponentHit(
	UPrimitiveComponent* HitComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface PhysMat = UGameplayStatics::GetSurfaceType(Hit);
		
		if (CurrentProjectileInfo.HitDecals.Contains(PhysMat))
		{
			UMaterialInterface* MatInt = CurrentProjectileInfo.HitDecals[PhysMat];

			if (MatInt && OtherActor)
			{
				UGameplayStatics::SpawnDecalAtLocation(GetWorld(), MatInt, FVector(20.0f, 20.0f, 10.0f), Hit.Location, Hit.ImpactNormal.Rotation(), 5.0f);
			}
		}
	}
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface PhysMat = UGameplayStatics::GetSurfaceType(Hit);
		
		if (CurrentProjectileInfo.HitDecalsFX.Contains(PhysMat))
		{
			UParticleSystem* DecalHitFX = CurrentProjectileInfo.HitDecalsFX[PhysMat];

			if (DecalHitFX && OtherActor)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DecalHitFX, Hit.Location, Hit.ImpactNormal.Rotation());
			}
		}
	}
	if (CurrentProjectileInfo.HitSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), CurrentProjectileInfo.HitSound, Hit.Location);
	}
	ImpactProjectile();
}

void ABulletDefault::BulletOnComponentBeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	//UE_LOG(DefaultBulletLog, Display, TEXT("ComponentOverlap!"));
}

void ABulletDefault::BulletOnComponentEndOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
}

void ABulletDefault::ImpactProjectile()
{
	//UE_LOG(DefaultBulletLog, Display, TEXT("PMC Impact InitialSpeed: %f"), ProjectileMovementComponent->InitialSpeed);
	this->Destroy();
}

