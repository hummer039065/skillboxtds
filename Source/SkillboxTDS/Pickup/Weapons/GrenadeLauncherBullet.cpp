// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillboxTDS/Pickup/Weapons/GrenadeLauncherBullet.h"

#include "DrawDebugHelpers.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(GrenadeLog, All, All);

AGrenadeLauncherBullet::AGrenadeLauncherBullet()
{

	PrimaryActorTick.bCanEverTick = true;

}

void AGrenadeLauncherBullet::BeginPlay()
{
	
	Super::BeginPlay();
	ImpactProjectile();

}

void AGrenadeLauncherBullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ExplosiveTimer(DeltaTime);
}

void AGrenadeLauncherBullet::BulletOnComponentHit(
	UPrimitiveComponent* HitComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	//Super::BulletOnComponentHit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
	
}

void AGrenadeLauncherBullet::ImpactProjectile()
{
	//Super::ImpactProjectile();
	bIsTimerEnable = true;
}

void AGrenadeLauncherBullet::ExplosiveTimer(float DeltaTime)
{
	if (bIsTimerEnable)
	{
		//UE_LOG(GrenadeLog, Display, TEXT("Timer: %f"), ActiveTime);
		if (ActiveTime >= TimerToExplosive)
		{
			Explode();
		}
		else
		{
			ActiveTime += DeltaTime;
		}
	}
}

void AGrenadeLauncherBullet::Explode()
{
	bIsTimerEnable = false;

	if (CurrentProjectileInfo.ExplosiveFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CurrentProjectileInfo.ExplosiveFX, GetActorTransform());
	}
	
	if (CurrentProjectileInfo.ExplosiveSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), CurrentProjectileInfo.HitSound, GetActorLocation());
	}
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), CurrentProjectileInfo.DamageExplosion, CurrentProjectileInfo.DamageExplosion, GetActorLocation(), CurrentProjectileInfo.CenterExplosion, CurrentProjectileInfo.RadiusExplosion, CurrentProjectileInfo.DamageFalloff, nullptr, IgnoreActors);
	DrawDebugSphere(GetWorld(), GetActorLocation(), CurrentProjectileInfo.RadiusExplosion, 16, FColor::Cyan, false, 3.0f);
	DrawDebugSphere(GetWorld(), GetActorLocation(), CurrentProjectileInfo.CenterExplosion, 16, FColor::Red, false, 3.0f);
	this->Destroy();
}
