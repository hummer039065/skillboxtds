// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillboxTDS/Pickup/Weapons/BulletDefault.h"
#include "GrenadeLauncherBullet.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOXTDS_API AGrenadeLauncherBullet : public ABulletDefault
{
	GENERATED_BODY()

public:

	AGrenadeLauncherBullet();
	
	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual void BulletOnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	
	virtual void ImpactProjectile() override;

	void ExplosiveTimer(float DeltaTime);

	void Explode();

	bool bIsTimerEnable = false;
	float TimerToExplosive = 2.0f;
	float ActiveTime = 0.0f;

	TArray<AActor*> IgnoreActors;
};
