// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponDefault.h"

#include "DrawDebugHelpers.h"
#include "SkillboxTDS/Pickup/Weapons/BulletDefault.h"
#include "SkillboxTDS/Character/SkillboxTDSCharacter.h"
#include "Components/ArrowComponent.h"
#include "Engine/StaticMeshActor.h"
#include "SkillboxTDS/Pickup/Weapons/Drops.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(WeaponLog, All, All);


// Sets default values
AWeaponDefault::AWeaponDefault()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(SceneComponent);

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetGenerateOverlapEvents(false);
	SkeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMesh->SetupAttachment(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMesh->SetupAttachment(SceneComponent);

	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootDirection"));
	Arrow->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon();
}

void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	DrawDispersionRadius(bShowTraceToCursor);
}

void AWeaponDefault::SetWeaponCanFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		bWeaponFiring = bIsFire;
	}
	else
	{
		bWeaponFiring = false;
		FireTimer = 0.01f;
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !bBlockFire;
}

bool AWeaponDefault::IsHasClip()
{
	return CurrentWeaponInfo.AmmoInfo.CurrentClips > 0;
}

bool AWeaponDefault::IsAmmoEmpty()
{
	return CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip == 0 && CurrentWeaponInfo.AmmoInfo.CurrentClips == 0;
}

bool AWeaponDefault::IsAmmoFull()
{
	return CurrentWeaponInfo.AmmoInfo.CurrentClips == CurrentWeaponInfo.AmmoInfo.MaxClips &&
		CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip == CurrentWeaponInfo.AmmoInfo.MaxAmmoInClip;
}

void AWeaponDefault::DecreaseAmmo()
{
	--CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip;
	
	OnUpdateWeaponInfo.Broadcast(CurrentWeaponInfo);
	
	//UE_LOG(WeaponLog, Display, TEXT("CurrentAmmo: %i"), CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip);
}

float AWeaponDefault::GetWeaponRound()
{
	return CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip;
}

int32 AWeaponDefault::GetCountBulletsInOneShot()
{
	BulletsInShot = CurrentWeaponInfo.NumbersOfBullets;
	return BulletsInShot;
}

bool AWeaponDefault::IsWeaponCanReload()
{
	return !IsAmmoEmpty();
}

void AWeaponDefault::InitReload()
{
	if (!IsHasClip() || IsAmmoEmpty() || bWeaponReloading) return;
	
	ReloadTimer = CurrentWeaponInfo.ReloadTime;
	bWeaponReloading = true;
	OnWeaponReloadStart.Broadcast(CurrentWeaponInfo.ReloadAnim);
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), CurrentWeaponInfo.SoundReloadWeapon, GetActorLocation());
	--CurrentWeaponInfo.AmmoInfo.CurrentClips;
	
	DropRound();
	UE_LOG(WeaponLog, Display, TEXT("Reload starting"));
}

void AWeaponDefault::FinishReload()
{
	bWeaponReloading = false;
	
	/*int32 AmmoNeedTaken = CurrentWeaponInfo.MaxRound;
	AmmoNeedTaken = AmmoNeedTaken - CurrentWeaponInfo.MaxRound;
	AdditionalWeaponInfo.AmmoRound = CurrentWeaponInfo.MaxRound;*/

	/*int32 AmmoNeedTaken = CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip;
	
	if (CurrentWeaponInfo.AmmoInfo.CurrentClips > 0)
	{
		AmmoNeedTaken = AmmoNeedTaken - CurrentWeaponInfo.AmmoInfo.MaxAmmoInClip; 
	}*/

	//int32 AmmoNeedTaken = CurrentWeaponInfo.AmmoInfo.MaxAmmoInClip;;
	//AmmoNeedTaken = AmmoNeedTaken - CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip;

	//--CurrentWeaponInfo.AmmoInfo.CurrentClips;
	CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip = CurrentWeaponInfo.AmmoInfo.MaxAmmoInClip;
	
	OnWeaponReloadEnd.Broadcast(true, CurrentWeaponInfo.AmmoInfo);
	OnUpdateWeaponInfo.Broadcast(CurrentWeaponInfo);
	
	UE_LOG(WeaponLog, Display, TEXT("Reload ending"));
}

void AWeaponDefault::CancelReload()
{
	bWeaponReloading = false;
	
	if (SkeletalMesh && SkeletalMesh->GetAnimInstance())
	{
		SkeletalMesh->GetAnimInstance()->StopAllMontages(0.15f);
	}
	
	OnWeaponReloadEnd.Broadcast(false, CurrentWeaponInfo.AmmoInfo);
	
}

FVector AWeaponDefault::GetFireEndLocation()
{
	FVector EndLocation = FVector::ZeroVector;
	FVector tmpV = (Arrow->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeFireLogic)
	{
		if (bAimToCursor)
		{
			EndLocation = CursorPosition;
		}
		else
		{
			EndLocation = Arrow->GetComponentLocation() + ApplyDispersionToShoot(Arrow->GetComponentLocation() - ShootEndLocation).GetSafeNormal() * -2000.0f;
		}
		
		if (DebugMode)
		{
			//DrawDebugCone(GetWorld(), Arrow->GetComponentLocation(), Arrow->GetForwardVector(), CurrentWeaponInfo.TraceDistance, GetCurrentDispersion() *  PI / 180, GetCurrentDispersion() *  PI / 180, 5, FColor::Cyan, false, 10.0f, 0.f, ConeThickness);
			DrawDebugLine(GetWorld(), Arrow->GetComponentLocation(), EndLocation, FColor::Magenta, false, 2.0f, 0, LineThickness);
		}
	}
	else
	{
		if (bAimToCursor)
		{
			EndLocation = CursorPosition;
		}
		else
		{
			EndLocation = Arrow->GetComponentLocation() + ApplyDispersionToShoot(Arrow->GetForwardVector()) * 2000.0f;
		}
	
		if (DebugMode)
		{
			//DrawDebugCone(GetWorld(), Arrow->GetComponentLocation(), Arrow->GetForwardVector(), CurrentWeaponInfo.TraceDistance, GetCurrentDispersion() *  PI / 180, GetCurrentDispersion() *  PI / 180, 5, FColor::Magenta, false, 10.0f, 0.f, ConeThickness);
			DrawDebugLine(GetWorld(), Arrow->GetComponentLocation(), EndLocation, FColor::Red, false, 2.0f, 0, LineThickness);
		}
	}
	
	return EndLocation;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector ForwardVector)
{
	const auto HalfRad = FMath::DegreesToRadians(CurrentDispersion);
	FVector Result = FMath::VRandCone(ForwardVector, /*GetCurrentDispersion() *  PI / 180*/ HalfRad);
	return Result;
}

float AWeaponDefault::GetCurrentDispersion()
{
	float Dispersion = CurrentDispersion;
	return Dispersion;
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return CurrentWeaponInfo.ProjectileInfo;
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (FireTimer < 0.0f)
	{
		if (GetWeaponRound() > 0)
		{
			if (!bWeaponReloading)
			{
				Fire();
			}
		}
		else
		{
			if (!bWeaponReloading && GetWeaponRound() != CurrentWeaponInfo.AmmoInfo.MaxAmmoInClip)
			{
				if (!IsHasClip()) return;
				
				InitReload();
				UE_LOG(WeaponLog, Display, TEXT("Init auto reload"));
			}
		}
	}
	else
	{
		FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bWeaponReloading)
	{
		if (ReloadTimer <= 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!bWeaponReloading)
	{
		if (!bWeaponFiring)
		{
			if (bShouldDispersionReduction)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction * DeltaTime;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction * DeltaTime;
			}
		}
		if (CurrentDispersion < CurrentMinDispersion)
		{
			CurrentDispersion = CurrentMinDispersion;
		}
		else if (CurrentDispersion > CurrentMaxDispersion)
		{
			CurrentDispersion = CurrentMaxDispersion;
		}
	}
	
}

void AWeaponDefault::Fire()
{
	if (bWeaponFiring)
	{
		ChangeDispersionByShot();
		FireTimer = CurrentWeaponInfo.RateOfFire;
		GetCountBulletsInOneShot();

		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), CurrentWeaponInfo.SoundFireWeapon, Arrow->GetComponentLocation());
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CurrentWeaponInfo.EffectFireWeapon, Arrow->GetComponentTransform());
		
		OnWeaponStartFire.Broadcast();
		
		if (Arrow)
		{
			for (int32 i = 0; i < BulletsInShot; i++)
			{
				auto ShootLocation = Arrow->GetComponentLocation();
				auto ShootRotation = Arrow->GetComponentRotation();
				FProjectileInfo BulletInfo;
				BulletInfo = GetProjectile();

				FVector ShootDirection = GetFireEndLocation() - ShootLocation;
				ShootDirection.Normalize();

				FMatrix MatrixDirection(ShootDirection, FVector(0.0f, 1.0f, 0.0f), FVector(0.0f, 0.0f, 1.0f), FVector::ZeroVector);
				ShootRotation = MatrixDirection.Rotator();

				if (CurrentWeaponInfo.ProjectileInfo.ProjectileTypes)
				{
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					//SpawnParameters.bDeferConstruction = true;
					SpawnParameters.Owner = GetOwner();
					SpawnParameters.Instigator = GetInstigator();
					
					ABulletDefault* Bullet = Cast<ABulletDefault>(GetWorld()->SpawnActor(CurrentWeaponInfo.ProjectileInfo.ProjectileTypes, &ShootLocation, &ShootRotation, SpawnParameters));

					if (Bullet)
					{
						Bullet->StaticMesh->SetStaticMesh(CurrentWeaponInfo.ProjectileInfo.ProjectileMesh);
						Bullet->ProjectileFX->SetTemplate(CurrentWeaponInfo.ProjectileInfo.TrialFX);
						Bullet->CurrentProjectileInfo = CurrentWeaponInfo.ProjectileInfo;
					}
				}
				else
				{
					FHitResult Hit;
					bool TraceLine;
					
					if (!bAimToCursor) //Flag to switch aim mode
					{
						TraceLine = GetWorld()->LineTraceSingleByChannel(Hit, ShootLocation, GetFireEndLocation(), ECollisionChannel::ECC_Visibility);
					}
					else
					{
						TraceLine = GetWorld()->LineTraceSingleByChannel(Hit, ShootLocation, CursorPosition, ECollisionChannel::ECC_Visibility);
					}
					
					if (TraceLine)
					{
						UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CurrentWeaponInfo.DecalOnHitMat, FVector(20.0f, 20.0f, 20.0f), Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), 5.0f);
						UGameplayStatics::SpawnSoundAtLocation(GetWorld(), CurrentWeaponInfo.ProjectileInfo.HitSound, Hit.Location);
					}
				}
			}
			
			DecreaseAmmo();
			ShellEject();
			
			//AdditionalWeaponInfo.AmmoRound -= BulletsInShot;
			
		}
	}
}

void AWeaponDefault::InitWeapon()
{
	//UE_LOG(WeaponLog, Display, TEXT(",WeaponDefault: Init Weapon"));
	
	if (SkeletalMesh && !SkeletalMesh->SkeletalMesh)
	{
		SkeletalMesh->DestroyComponent(true);
	}
	if (StaticMesh && !StaticMesh->GetStaticMesh())
	{
		StaticMesh->DestroyComponent(true);
	}
	GetWeaponRound();
	
	OnUpdateWeaponInfo.Broadcast(CurrentWeaponInfo);
}

void AWeaponDefault::UpdateWeaponState(EMovementState MovementState)
{
	bBlockFire = false;
	switch (MovementState)
	{
	case EMovementState::AimWalk_State:
		CurrentMinDispersion = CurrentWeaponInfo.FWeaponDispersion.AimWalkStateMinDispersion;
		CurrentMaxDispersion = CurrentWeaponInfo.FWeaponDispersion.AimWalkStateMaxDispersion;
		CurrentDispersionRecoil = CurrentWeaponInfo.FWeaponDispersion.AimWalkStateRecoilDispersion;
		CurrentDispersionReduction = CurrentWeaponInfo.FWeaponDispersion.AimWalkStateReductionDispersion;
		break;
	case EMovementState::Aim_State:
		CurrentMinDispersion = CurrentWeaponInfo.FWeaponDispersion.AimStateMinDispersion;
		CurrentMaxDispersion = CurrentWeaponInfo.FWeaponDispersion.AimStateMaxDispersion;
		CurrentDispersionRecoil = CurrentWeaponInfo.FWeaponDispersion.AimStateRecoilDispersion;
		CurrentDispersionReduction = CurrentWeaponInfo.FWeaponDispersion.AimStateReductionDispersion;
		break;
	case EMovementState::Walk_State:
		CurrentMinDispersion = CurrentWeaponInfo.FWeaponDispersion.WalkStateMinDispersion;
		CurrentMaxDispersion = CurrentWeaponInfo.FWeaponDispersion.WalkStateMaxDispersion;
		CurrentDispersionRecoil = CurrentWeaponInfo.FWeaponDispersion.WalkStateRecoilDispersion;
		CurrentDispersionReduction = CurrentWeaponInfo.FWeaponDispersion.WalkStateReductionDispersion;
		break;
	case EMovementState::Run_State:
		CurrentMinDispersion = CurrentWeaponInfo.FWeaponDispersion.RunStateMinDispersion;
		CurrentMaxDispersion = CurrentWeaponInfo.FWeaponDispersion.RunStateMaxDispersion;
		CurrentDispersionRecoil = CurrentWeaponInfo.FWeaponDispersion.RunStateRecoilDispersion;
		CurrentDispersionReduction = CurrentWeaponInfo.FWeaponDispersion.RunStateReductionDispersion;
		break;
	case EMovementState::Sprint_State:
		bBlockFire = true;
		SetWeaponCanFire(bBlockFire);
		break;
	default: ;
	}
}

void AWeaponDefault::DropRound()
{
	if (CurrentWeaponInfo.MagazineDrop == nullptr) return;
	
	FVector DropRoundLocation = SkeletalMesh->GetSocketLocation("MagazineDrop");
	FRotator DropRoundRotarion = SkeletalMesh->GetSocketRotation("MagazineDrop");
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	
	auto Round = Cast<ADrops>(GetWorld()->SpawnActor(RoundDropClass, &DropRoundLocation, &DropRoundRotarion, SpawnParameters));
	if (Round)
	{
		Round->RoundDropModel->SetStaticMesh(CurrentWeaponInfo.MagazineDrop);
	}
}

void AWeaponDefault::ShellEject()
{
	if (CurrentWeaponInfo.SleeveBullets == nullptr) return;

	FVector DropRoundLocation = SkeletalMesh->GetSocketLocation("AmmoEject");
	FRotator DropRoundRotarion = SkeletalMesh->GetSocketRotation("AmmoEject");
	float RandImpulse = FMath::RandRange(50.0f, 75.0f);
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	
	auto Shell = Cast<ADrops>(GetWorld()->SpawnActor(RoundDropClass, &DropRoundLocation, &DropRoundRotarion, SpawnParameters));
	if (Shell)
	{
		Shell->RoundDropModel->SetStaticMesh(CurrentWeaponInfo.SleeveBullets);
		Shell->RoundDropModel->AddImpulse(SkeletalMesh->GetForwardVector() * -RandImpulse);
	}
}

void AWeaponDefault::DrawDispersionRadius(bool bIsActive)
{
	if (bIsActive)
	{
		if (bAimToCursor)
		{
			DrawDebugLine(GetWorld(), Arrow->GetComponentLocation(), CursorPosition, FColor::Blue, false, 0.f, 0, LineThickness);
		}
		else
		{
			DrawDebugLine(GetWorld(), Arrow->GetComponentLocation(), FVector(CursorPosition.X, CursorPosition.Y, CursorPosition.Z + 120.0f), FColor::Green, false, 0.f, 0, LineThickness);
		}
	}
}

