// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Drops.generated.h"

UCLASS()
class SKILLBOXTDS_API ADrops : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADrops();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Drop")
	USphereComponent* Collision;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Drop")
	UStaticMeshComponent* RoundDropModel = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Drop")
	UStaticMeshComponent* ShellDropModel = nullptr;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Drop")
	float SphereRadius = 10.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
