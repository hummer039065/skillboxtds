// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "SkillboxTDS/Pickup/Weapons/Drops.h"
#include "GameFramework/Actor.h"
#include "SkillboxTDS/FuncLibrary/Types.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Animation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, FAmmoInfo, AmmoInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponStartFire);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponStopFire);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateWeaponInfo, const FWeaponInfo&, WeaponInfo);



class UArrowComponent;


UCLASS()
class SKILLBOXTDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnUpdateWeaponInfo OnUpdateWeaponInfo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	USceneComponent* SceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	USkeletalMeshComponent* SkeletalMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	UStaticMeshComponent* StaticMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	UArrowComponent* Arrow = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FWeaponInfo CurrentWeaponInfo;
	
	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	//FAdditionalWeaponInfo AdditionalWeaponInfo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FName WeaponName = "";

	FProjectileInfo GetProjectile();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ADrops> RoundDropClass;

	UPROPERTY(BlueprintReadWrite, Category = "FireLogic")
	bool bWeaponFiring = false;
	UPROPERTY(BlueprintReadWrite, Category = "FireLogic")
	bool bWeaponReloading = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	bool CheckWeaponCanFire();

	bool IsHasClip();
	bool IsAmmoEmpty();
	bool IsAmmoFull();
	void DecreaseAmmo();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void Fire();

	FOnWeaponStartFire OnWeaponStartFire;
	FOnWeaponStopFire OnWeaponStopFire;

	bool bBlockFire = false;

	FVector ShootEndLocation = FVector::ZeroVector;

	void InitWeapon();

	void SetWeaponCanFire(bool bIsFire);

	float FireTimer = 0.0f;
	UPROPERTY(BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	UPROPERTY(BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTime;

	UFUNCTION(BlueprintCallable)
	float GetWeaponRound();

	UFUNCTION(BlueprintCallable)
	int32 GetCountBulletsInOneShot();

	bool IsWeaponCanReload();
	void InitReload();
	void FinishReload();
	void CancelReload();

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Debug")
	bool byBarrel = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Debug")
	bool DebugMode = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeFireLogic = 100.f;

	FVector GetFireEndLocation();
	FVector ApplyDispersionToShoot(FVector ForwardVector);

	//Dispersion
	bool bShouldDispersionReduction = false;
	float CurrentDispersion = 1.0f;
	float CurrentMinDispersion = 0.1f;
	float CurrentMaxDispersion = 2.0f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	UFUNCTION(BlueprintCallable, Category = "DebugDispersion")
	float GetCurrentDispersion();
	
	void ChangeDispersionByShot();

	UFUNCTION(BlueprintCallable)
	void UpdateWeaponState(EMovementState MovementState);

	//Drops
	void DropRound();
	void ShellEject();
	
	//Debug
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool bAimToCursor = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool bShowTraceToCursor = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug", meta = (EditCondition = "bShowTraceToCursor"))
	float ConeThickness = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug", meta = (EditCondition = "bShowTraceToCursor"))
	float LineThickness = 0.2f;

	void DrawDispersionRadius(bool bIsActive);

	FVector CursorPosition = FVector::ZeroVector;

private:

	int32 BulletsInShot = 1;




};



