// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillboxTDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkillboxTDS, "SkillboxTDS" );

DEFINE_LOG_CATEGORY(LogSkillboxTDS)
 