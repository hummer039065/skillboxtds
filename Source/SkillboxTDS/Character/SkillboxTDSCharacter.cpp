// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillboxTDSCharacter.h"
#include "Camera/CameraComponent.h"
#include "SkillboxTDS/Pickup/Weapons/WeaponDefault.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"
#include "SkillboxTDS/FuncLibrary/InventoryComponent.h"
#include "SkillboxTDS/Game/SkillboxTDSGameInstance.h"

DEFINE_LOG_CATEGORY_STATIC(CharacterLog, All, All);

ASkillboxTDSCharacter::ASkillboxTDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-45.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	
	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	CurrentZoom = CameraBoom->TargetArmLength;

	Inventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory"));

	if (Inventory)
	{
		Inventory->OnSwitchWeapon.AddDynamic(this, &ASkillboxTDSCharacter::InitWeapon);
	}
}

void ASkillboxTDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* PlayerController = Cast<APlayerController>(GetController());

		if (PlayerController)
		{
			FHitResult Result;
			PlayerController->GetHitResultUnderCursor(ECC_Visibility, true, Result);
			FVector CursorVector = Result.ImpactNormal;
			FRotator CursorRotator = CursorVector.Rotation();

			CurrentCursor->SetWorldLocation(Result.Location);
			CurrentCursor->SetWorldRotation(CursorRotator);
			//CurrentWeapon->CursorPosition = CurrentCursor->GetComponentLocation();
		}
	}
	MovementTick(DeltaSeconds);
}

void ASkillboxTDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		 CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0,0,0));
	}


	
}

void ASkillboxTDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASkillboxTDSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASkillboxTDSCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Zoom", this, &ASkillboxTDSCharacter::CameraZoomUpdate);
	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ASkillboxTDSCharacter::Aiming);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &ASkillboxTDSCharacter::Aiming);
	PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &ASkillboxTDSCharacter::Walking);
	PlayerInputComponent->BindAction("Walk", IE_Released, this, &ASkillboxTDSCharacter::Walking);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASkillboxTDSCharacter::Sprinting);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASkillboxTDSCharacter::Sprinting);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASkillboxTDSCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASkillboxTDSCharacter::StopFire);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ASkillboxTDSCharacter::Reload);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &ASkillboxTDSCharacter::SwitchNextWeapon);
	PlayerInputComponent->BindAction("PrevWeapon", IE_Pressed, this, &ASkillboxTDSCharacter::SwitchPrevWeapon);
}

void ASkillboxTDSCharacter::Aiming()
{
	if (bisActiveState && !bIsAiming) return;

	auto CurrentSpeedX = GetCharacterMovement()->Velocity.X;
	auto CurrentSpeedY = GetCharacterMovement()->Velocity.Y;
	
	if (!bIsAiming)
	{
		if (CurrentSpeedX <= 0 && CurrentSpeedY <= 0)
		{
			bIsAiming = true;
			bisActiveState = true;
			DefaultMovementState = EMovementState::Aim_State;
			CharacterUpdate();
		}
		if (CurrentSpeedX > 0 || CurrentSpeedY > 0)
		{
			bIsAiming = true;
			bisActiveState = true;
			DefaultMovementState = EMovementState::AimWalk_State;
			CharacterUpdate();
		}
	}
	else if (bIsAiming && bisActiveState)
	{
		bIsAiming = false;
		bisActiveState = false;
		DefaultMovementState = EMovementState::Run_State;
	}
	CharacterUpdate();
}

void ASkillboxTDSCharacter::Walking()
{
	if (bisActiveState && !bIsWalking) return;

	if (!bIsWalking)
	{
		bIsWalking = true;
		bisActiveState = true;
		DefaultMovementState = EMovementState::Walk_State;
	}
	else if (bIsWalking && bisActiveState)
	{
		bIsWalking = false;
		bisActiveState = false;
		DefaultMovementState = EMovementState::Run_State;
	}
	CharacterUpdate();
}

void ASkillboxTDSCharacter::Sprinting()
{
	if (bisActiveState && !bisSprinting) return;

	if (!bisSprinting)
	{
		bisSprinting = true;
		bisActiveState = true;
		DefaultMovementState = EMovementState::Sprint_State;
		
	}
	else if (bisSprinting && bisActiveState)
	{
		bisSprinting = false;
		bisActiveState = false;
		DefaultMovementState = EMovementState::Run_State;
	}
	CharacterUpdate();
}

void ASkillboxTDSCharacter::MoveForward(float value)
{
	AxisX = value;
}

void ASkillboxTDSCharacter::MoveRight(float value)
{
	AxisY = value;
}

void ASkillboxTDSCharacter::MovementTick(float DeltaTime)
{
	if (bisSprinting)
	{
		FVector FwdVector = GetActorForwardVector();
		AddMovementInput(FwdVector, 1.0f);
	}
	else
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
	}

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(),0);
	
	if (MyController)
	{
		FVector WorldLocation;
		FVector WorldDirection;
		float DistanceAboveGround = 0.0f;

		MyController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection); 
		FVector PlaneOrigin(0.0f, 0.0f, DistanceAboveGround);
		FVector ActorWorldLocation = FMath::LinePlaneIntersection(WorldLocation, WorldLocation + WorldDirection, PlaneOrigin, FVector::UpVector);

		FHitResult Result;
		MyController->GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, true, Result);
		float RotatorResult = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ActorWorldLocation).Yaw;
		SetActorRotation(FRotator(0.0, RotatorResult, 0.0));
		CurrentWeapon->ShootEndLocation = Result.Location + Displacement;
		CurrentWeapon->CursorPosition = Result.Location;
	}
}

void ASkillboxTDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	
	switch (DefaultMovementState)
	{
	case EMovementState::AimWalk_State:
		ResSpeed = CharacterSpeedInfo.AimWalkSpeed;
		Displacement = FVector(0.0f, 0.0f, 160.f);
		CurrentWeapon->bShouldDispersionReduction = true;
		break;
	case EMovementState::Aim_State:
		ResSpeed = CharacterSpeedInfo.AimSpeed;
		Displacement = FVector(0.0f, 0.0f, 160.f);
		CurrentWeapon->bShouldDispersionReduction = true;
		break;
	case EMovementState::Walk_State:
		ResSpeed = CharacterSpeedInfo.WalkSpeed;
		Displacement = FVector(0.0f, 0.0f, 120.f);
		CurrentWeapon->bShouldDispersionReduction = false;
		break;
	case EMovementState::Run_State:
		ResSpeed = CharacterSpeedInfo.RunSpeed;
		Displacement = FVector(0.0f, 0.0f, 120.f);
		CurrentWeapon->bShouldDispersionReduction = false;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = CharacterSpeedInfo.SprintSpeed;
		break;
	default: ;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	
	if (CurrentWeapon)
	{
		CurrentWeapon->UpdateWeaponState(DefaultMovementState);
	}
}

void ASkillboxTDSCharacter::ChangeCharacterState(EMovementState NewMovementState)
{
	DefaultMovementState = NewMovementState;
	CharacterUpdate();
}

void ASkillboxTDSCharacter::CameraZoomUpdate(float Value)
{
	if (Value == 0.0f) return;
	if (Value > 0.0f && CurrentZoom > MinCameraHeight)
	{
		CameraZoomCounter += CameraZoomRate;
		GetWorldTimerManager().SetTimer(CameraHandle, this, &ASkillboxTDSCharacter::CameraZoomIn, 0.01f, true);
	}
	else if (Value < 0.0f && CurrentZoom < MaxCameraHeight)
	{
		CameraZoomCounter -= CameraZoomRate;
		GetWorldTimerManager().SetTimer(CameraHandle, this, &ASkillboxTDSCharacter::CameraZoomOut, 0.01f, true);
	}
}

UDecalComponent* ASkillboxTDSCharacter::GetCursorToWorld()
{
		return CurrentCursor;
}

void ASkillboxTDSCharacter::CameraZoomIn()
{
	if (CameraZoomCounter > 0.0f)
	{
		CurrentZoom = FMath::Clamp(CurrentZoom - 10.0f, MinCameraHeight, MaxCameraHeight);
		CameraBoom->TargetArmLength = CurrentZoom;
		CameraZoomCounter--;
	}
	else
	{
		GetWorldTimerManager().ClearTimer(CameraHandle);
	}
}

void ASkillboxTDSCharacter::CameraZoomOut()
{
	if (CameraZoomCounter < 0.0f)
	{
		CurrentZoom = FMath::Clamp(CurrentZoom + 10.0f, MinCameraHeight, MaxCameraHeight);
		CameraBoom->TargetArmLength = CurrentZoom;
		CameraZoomCounter++;
	}
	else
	{
		GetWorldTimerManager().ClearTimer(CameraHandle);
	}
}

AWeaponDefault* ASkillboxTDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ASkillboxTDSCharacter::InitWeapon(FName WeaponName, const FAmmoInfo& AmmoInfo)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	USkillboxTDSGameInstance* MyGI = Cast<USkillboxTDSGameInstance>(GetGameInstance());

	if (MyGI)
	{
		MyGI->PrintText(WeaponName);
		
		if (MyGI->GetWeaponInfoByTableName(WeaponName, MyWeaponInfo))
		{
			if (MyWeaponInfo.WeaponType)
			{
				FVector SpawnLocation = FVector::ZeroVector;
				FRotator SpawnRotation = FRotator::ZeroRotator;

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* NewCurrentWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponType, &SpawnLocation, &SpawnRotation, SpawnParams));

				UE_LOG(CharacterLog, Display, TEXT("Creating a weapon"));
				if (NewCurrentWeapon)
				{
					UE_LOG(CharacterLog, Display, TEXT("Weapon, created, trying to attach"));
					FAttachmentTransformRules TransformRules(EAttachmentRule::SnapToTarget, false);
					NewCurrentWeapon->AttachToComponent(GetMesh(), TransformRules, FName("WeaponSocket"));
					CurrentWeapon = NewCurrentWeapon;
					
					NewCurrentWeapon->CurrentWeaponInfo = MyWeaponInfo;
					//NewCurrentWeapon->AdditionalWeaponInfo.AmmoRound = AdditionalWeaponInfo.AmmoRound;
					NewCurrentWeapon->CurrentWeaponInfo.AmmoInfo.CurrentAmmoInClip = AmmoInfo.CurrentAmmoInClip;
					NewCurrentWeapon->ReloadTime = MyWeaponInfo.ReloadTime;
					NewCurrentWeapon->CurrentWeaponInfo.HipFireAnim = MyWeaponInfo.HipFireAnim;
					NewCurrentWeapon->UpdateWeaponState(DefaultMovementState);
					if (Inventory)
					{
						UE_LOG(CharacterLog, Display, TEXT("Inventory Ok"));
						CurrentWeaponIndex = Inventory->GetWeaponSlotIndexByName(WeaponName);
						
						UE_LOG(CharacterLog, Display, TEXT("CurrentWeaponIndex: %i"), CurrentWeaponIndex);
					}
					NewCurrentWeapon->OnWeaponReloadStart.AddDynamic(this, &ASkillboxTDSCharacter::OnWeaponReloadStart);
					NewCurrentWeapon->OnWeaponReloadEnd.AddDynamic(this, &ASkillboxTDSCharacter::OnWeaponReloadEnd);
					NewCurrentWeapon->OnWeaponStartFire.AddDynamic(this, &ASkillboxTDSCharacter::OnWeaponStartFire);
					NewCurrentWeapon->OnUpdateWeaponInfo.AddDynamic(Inventory, &UInventoryComponent::UpdateWeaponInfo);
					CharacterUpdate();
				}
			}
		}
	}
}

void ASkillboxTDSCharacter::StartFire()
{
	CanAttack(true);
}

void ASkillboxTDSCharacter::OnWeaponStartFire()
{
	if (Inventory && CurrentWeapon)
	{
		Inventory->SetWeaponInfoByIndex(CurrentWeaponIndex, CurrentWeapon->CurrentWeaponInfo.AmmoInfo);
	}
	OnWeaponStartFire_BP();
}

void ASkillboxTDSCharacter::StopFire()
{
	CanAttack(false);
}

void ASkillboxTDSCharacter::Reload()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->CurrentWeaponInfo.AmmoInfo.MaxAmmoInClip && CurrentWeapon->bWeaponReloading != true)
		{
			CurrentWeapon->InitReload();
		}
	}
}

void ASkillboxTDSCharacter::OnWeaponReloadStart(UAnimMontage* ReloadAnimation)
{
	OnWeaponReloadStart_BP(ReloadAnimation);
	bIsReloading = true;
}

void ASkillboxTDSCharacter::OnWeaponReloadEnd(bool bIsSuccess, FAmmoInfo AmmoInfo)
{
	/*if (Inventory && CurrentWeapon)
	{
		Inventory->WeaponChangeAmmo(CurrentWeapon->CurrentWeaponInfo.WeaponTypeAmmo, AmmoInfo);
	}*/
	OnWeaponReloadEnd_BP_Implementation(bIsSuccess);
	bIsReloading = false;
}

void ASkillboxTDSCharacter::SwitchNextWeapon()
{
	UE_LOG(CharacterLog, Display, TEXT("NextWeapon!"));
	if (Inventory->WeaponSlots.Num() > 1)
	{
		int32 OldIndex = CurrentWeaponIndex;
		FAmmoInfo OldAmmoInfo;
		if (CurrentWeapon)
		{
			OldAmmoInfo = CurrentWeapon->CurrentWeaponInfo.AmmoInfo;
			if (CurrentWeapon->bWeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (Inventory)
		{
			Inventory->SwitchWeaponToIndex(CurrentWeaponIndex + 1, OldIndex, OldAmmoInfo);
			IdWeaponName = CurrentWeapon->WeaponName;
			UE_LOG(CharacterLog, Display, TEXT("NextWeapon! OldIndex: %i OldInfo: %i"), OldIndex, OldAmmoInfo.CurrentAmmoInClip);
		}
	}
}

void ASkillboxTDSCharacter::SwitchPrevWeapon()
{
	UE_LOG(CharacterLog, Display, TEXT("PrevWeapon!"));
	if (Inventory->WeaponSlots.Num() > 1)
	{
		int32 OldIndex = CurrentWeaponIndex;
		FAmmoInfo OldAmmoInfo;
		if (CurrentWeapon)
		{
			OldAmmoInfo = CurrentWeapon->CurrentWeaponInfo.AmmoInfo;
			if (CurrentWeapon->bWeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (Inventory)
		{
			Inventory->SwitchWeaponToIndex(CurrentWeaponIndex - 1, OldIndex, OldAmmoInfo);
			IdWeaponName = CurrentWeapon->WeaponName;
			UE_LOG(CharacterLog, Display, TEXT("PrevWeapon! OldIndex: %i OldInfo: %i"), OldIndex, OldAmmoInfo.CurrentAmmoInClip);
		}
	}

}

void ASkillboxTDSCharacter::CanAttack(bool bIsFire)
{
	AWeaponDefault* MyWeapon = nullptr;
	MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponCanFire(bIsFire);
	}
	
}
