// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SkillboxTDS/FuncLibrary/Types.h"
#include "SkillboxTDS/Pickup/Weapons/WeaponDefault.h"
#include "SkillboxTDSCharacter.generated.h"

class UWidgetLayoutLibrary;
class UInventoryComponent;


UCLASS(Blueprintable)
class ASkillboxTDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASkillboxTDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	void BeginPlay();

private:
	/** Top down camera */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;
	
	UPROPERTY()
	AWeaponDefault* CurrentWeapon = nullptr;


public:

	// Cursor
	float AxisX;
	float AxisY;

	UPROPERTY()
	UDecalComponent* CurrentCursor = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UMaterialInstance* CursorMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Inventory")
	UInventoryComponent* Inventory;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FVector CursorSize = FVector(20,20,0);

	UFUNCTION()
	void CameraZoomUpdate(float Value);

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	// CameraControl

	FTimerHandle CameraHandle;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CameraControl")
	float CurrentZoom;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CameraControl")
	float MaxCameraHeight = 1000.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CameraControl")
	float MinCameraHeight = 500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="CameraControl")
	float CameraZoomRate = 10.f;

	float CameraZoomCounter = 0.0f;

	void CameraZoomIn();
	void CameraZoomOut();

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D Coord;

	// State info
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="StateInfo")
	bool bIsRunning = true;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="StateInfo")
	bool bIsWalking = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="StateInfo")
	bool bIsAiming = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="StateInfo")
	bool bIsReloading = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="StateInfo")
	bool bisSprinting = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="StateInfo")
	bool bisActiveState = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="StateInfo")
	bool bIsFiring = false;

	UFUNCTION()
	void Walking();
	UFUNCTION(BlueprintCallable)
	void Aiming();
	UFUNCTION()
	void Sprinting();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	EMovementState DefaultMovementState = EMovementState::Run_State;

	FVector Displacement = FVector(0);

	UPROPERTY()
	FCharacterSpeed CharacterSpeedInfo;

	UFUNCTION()
	void MoveForward(float Value);
	UFUNCTION()
	void MoveRight(float Value);
	
	UFUNCTION()
	void MovementTick(float DeltaTime);
	
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	
	UFUNCTION(BlueprintCallable)
	void ChangeCharacterState(EMovementState NewMovementState);
	
	//Weapon

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FName IdWeaponName = "Null";

	FWeaponInfo MyWeaponInfo;

	int8 CurrentWeaponIndex = 0;
	
	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	
	UFUNCTION()
	void InitWeapon(FName WeaponName, const FAmmoInfo& AmmoInfo);

	void Reload();

	UFUNCTION()
	void OnWeaponReloadStart(UAnimMontage* ReloadAnimation);
	UFUNCTION(BlueprintImplementableEvent)
	void OnWeaponReloadStart_BP(UAnimMontage* ReloadAnimation);
	UFUNCTION()
	void OnWeaponReloadEnd(bool bIsSuccess, FAmmoInfo AmmoInfo);
	UFUNCTION(BlueprintImplementableEvent)
	void OnWeaponReloadEnd_BP_Implementation(bool bIsSuccess);

	void SwitchNextWeapon();
	void SwitchPrevWeapon();

protected:

	UFUNCTION()
	void StartFire();

	UFUNCTION()
	void OnWeaponStartFire();
	UFUNCTION(BlueprintImplementableEvent)
	void OnWeaponStartFire_BP();

	UFUNCTION()
	void StopFire();

	// Support function
	UFUNCTION(BlueprintCallable)
	void CanAttack(bool bIsFire);

	bool IsAmmoFull() const;
	bool IsCanReload() const;
	
};

