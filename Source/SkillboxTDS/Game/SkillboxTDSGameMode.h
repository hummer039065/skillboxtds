// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillboxTDSGameMode.generated.h"

UCLASS(minimalapi)
class ASkillboxTDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASkillboxTDSGameMode();
};



