// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillboxTDSGameMode.h"
#include "SkillboxTDSPlayerController.h"
#include "SkillboxTDS/Character/SkillboxTDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASkillboxTDSGameMode::ASkillboxTDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASkillboxTDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	/*static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("D:/Epic Games/Epic Games/UE projects/SkillboxTDS/Content/Blueprints/Characters/BP_Character.uasset"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/
}
