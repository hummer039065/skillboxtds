// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillboxTDS/Game/SkillboxTDSGameInstance.h"

DEFINE_LOG_CATEGORY_STATIC(GameInstanceLog, All, All);

USkillboxTDSGameInstance::USkillboxTDSGameInstance()
{
	//static ConstructorHelpers::FClassFinder<UDataTable> NewDataTable1(TEXT("DataTable'/Game/Blueprints/Game/NewDataTable1.NewDataTable1'"));

}

bool USkillboxTDSGameInstance::GetWeaponInfoByTableName(FName WeaponNameTable, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	if (WeaponInfoTable)
	{
		static const FString Context(TEXT("ContextString"));
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponNameTable, Context, false);
		//UE_LOG(GameInstanceLog, Display, TEXT("WeaponInfoRow: %f"), WeaponInfoRow->RateOfFire);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
			UE_LOG(GameInstanceLog, Display, TEXT("WeaponInfoRow found!"));
		}
	}
	
	return bIsFind;
}


void USkillboxTDSGameInstance::PrintText(FName SomeWeaponNameTable)
{
	UE_LOG(GameInstanceLog, Display, TEXT("Ok, i'm in GI, Weapon name is: %s "),*SomeWeaponNameTable.ToString());

}


