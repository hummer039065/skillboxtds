// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "SkillboxTDS/FuncLibrary/Types.h"
#include "SkillboxTDSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOXTDS_API USkillboxTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	USkillboxTDSGameInstance();

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DataWeaponInfo")
	UDataTable* WeaponInfoTable = nullptr;
	
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByTableName(FName WeaponNameTable, FWeaponInfo& OutInfo);
	
	UFUNCTION(BlueprintCallable)
	void PrintText(FName SomeWeaponNameTable);

	
	FWeaponInfo* WeaponInfoRow;

	
	
};
